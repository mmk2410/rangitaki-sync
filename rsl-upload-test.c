/*
 * Rangitaki Sync Library
 *
 * Upload Test / Example
 *
 * A program for downloading and uploading blog posts,
 * blogs file and media files from a rangitaki blog.
 *
 * Proudly written in C and with use of libssh (libssh.org)
 *
 * Version: 0.1
 *
 * COPYRIGHT (c) 2015 - 2016 The Rangitaki Project
 * COPYRIGHT (c) 2015 - 2016 Marcel Kapfer (mmk2410)
 *                           <marcelmichaelkapfer@yahoo.co.nz>
 *
 * MIT License
 *
 */


 #include <stdio.h>
 #include <stdlib.h>
 #include <sys/stat.h>
 #include <sys/types.h>
 #include <libssh/libssh.h>

 #include "rangitaki-sync.h"
 #include "dbg.h"

 #define MAX_DATA 512

 int run(const char *host, const char *user, const char *password,
            const char *remote_dir, const char *local_dir, const int port)
 {
     // Print the ssh struct
     ssh_data *data = malloc(sizeof(ssh_data));
     ssh_session ssh;
     ssh_scp scp;
     int rc;

     ssh_initialize(host, user, password,
             remote_dir, local_dir, port, data);

     mkdir(data->local_dir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

     ssh = ssh_open(data);
     check(ssh != NULL, "Error while connecting to the server.");

     scp = scp_open(ssh, data, SSH_SCP_WRITE);
     check(scp != NULL, "Error while creating a scp connection");

     rc = scp_upload(ssh, scp, data);
     check(rc == 0, "Error while uploading");


     scp_close(scp);

     ssh_close(ssh);

     free(data->host);
     free(data->user);
     free(data->password);
     free(data->remote_dir);
     free(data->local_dir);
     free(data);
     return 0;

 error:
     if(scp) scp_close(scp);
     if(ssh) ssh_close(ssh);

     free(data->host);
     free(data->user);
     free(data->password);
     free(data->remote_dir);
     free(data->local_dir);
     free(data);
     return -1;
 }

int main(int argc, char *argv[])
{
    check(argc == 1, "Too much arguments");

    printf("RANGITAKI SYNC LIBRARY\n");
    printf("Version: 0.1\n");
    printf("COPYRIGHT (c) 2015 The Rangitaki Project\n");
    printf("COPYRIGHT (c) 2015 Marcel Kapfer (mmk2410) <marcelmichaelkapfer@yahoo.co.nz>\n");
    printf("MIT License\n\n");
    printf("RSL Upload test\n\n");

    char *in = NULL;
    int rc = 0;
    char host[MAX_DATA];
    char user[MAX_DATA];
    char password[MAX_DATA];
    char remote_dir[MAX_DATA];
    char local_dir[MAX_DATA];
    int port;

    printf("Host: ");
    in = fgets(host, MAX_DATA - 1, stdin);
    strtok(host, "\n");
    check(in != NULL, "Failed to read host.");

    printf("User: ");
    in = fgets(user, MAX_DATA - 1, stdin);
    strtok(user, "\n");
    check(in != NULL, "Failed to read host.");

    printf("Password: ");
    in = fgets(password, MAX_DATA - 1, stdin);
    strtok(password, "\n");
    check(in != NULL, "Failed to read password.");

    printf("Remote Directory: ");
    in = fgets(remote_dir, MAX_DATA - 1, stdin);
    strtok(remote_dir, "\n");
    check(in != NULL, "Failed to read directory.");

    printf("Local File: ");
    in = fgets(local_dir, MAX_DATA - 1, stdin);
    strtok(local_dir, "\n");
    check(in != NULL, "Failed to read file");

    printf("Port: ");
    rc = fscanf(stdin, "%i", &port);
    check(rc > 0, "Failed to read port.");

    printf("\nRunning Upload now.\n\n");

    run(host, user, password, remote_dir, local_dir, port);

    return 0;

error:
    return -1;
}
