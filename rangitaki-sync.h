/*
 * Rangitaki Sync Library
 *
 * A program for downloading and uploading files over ssh.
 * Written for the Rangitaki blogging engine.
 *
 * Proudly written in C and with use of libssh (libssh.org)
 *
 * Version: 0.1
 *
 * COPYRIGHT (c) 2015 - 2016 The Rangitaki Project
 * COPYRIGHT (c) 2015 - 2016 Marcel Kapfer (mmk2410)
 *                           <marcelmichaelkapfer@yahoo.co.nz>
 *
 * MIT License
 *
 */

#ifndef _rangitakisync_h
#define _rangitakisync_h

#include <libssh/libssh.h>

typedef struct{
    char *host;
    char *user;
    char *password;
    char *remote_dir;
    char *local_dir;
    unsigned int port;
    unsigned int verbosity;
} ssh_data;


void ssh_initialize(const char *host, const char *user, const char *password,
        const char *remote_dir, const char *local_dir,
        const unsigned int port, ssh_data *data);
ssh_session ssh_open(ssh_data *data);
int ssh_close(ssh_session ssh);
ssh_scp scp_open(ssh_session ssh, ssh_data *data, int mode);
int scp_close(ssh_scp scp);
int scp_download(ssh_session ssh, ssh_scp scp, ssh_data *data);
int scp_upload(ssh_session ssh, ssh_scp scp, ssh_data *data);

#endif
