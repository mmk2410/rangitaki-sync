# Rangitaki Sync Library

First of all: **I'm really sorry** for my bad code style, the bad design, all the ugliness , the missing comments (I will add them as soon as possible), for some things that may not work as expected, for every C developer who is used to read good code, for all the bugs that may happen. I'm sorry, but I'm not a good C developer. I just started coding in that language.

But, however, the funny part is: IT ACTUALLY WORKS! (or at least it should and did in my tests)

Anyway, this is a library written for synchronizing a Rangitaki blog with your device. (Actually you can download every directory recursive and upload single files...).

For testing purposes I wrote two test programs: One for uploading (single file), one for downloading (recursive, into /tmp).

*These library is written on Linux and tested on Linux. It may works on other systems, but I don't know.*

Even if I write all of my programs in the hope, that they will be useful for someone one day, I don't think that this will be used by anyone else than me. But look also in the 'Useful Stuff' section of this README.

## Test programs

After running `make` you have following programs:

### rsl-download-test

Just run it with `./rsl-download-test`. You don't need to give any arguments, since it will ask you everything.

This program will download you all files and subdirectory of the entered remote directory into `/tmp/rangitaki-sync`. Normally the program should create this directory, if it isn't already there. If it fails to create it and it isn't already there, you will receive a nice and friendly error message...

## rsl-upload-test

Just run it with `./rsl-upload-test`. You don't need to give any arguments, since it will ask you everything.

This program will upload a single file form the entered path to the entered path on your server.

## Credits

### libssh project ([libssh.org](http://libssh.org))

For there great ssh library and the awesome documentation.

### Zed A. Shaw ([zedshaw.com](http://zedshaw.com))

For his great book ["Learn C the hard way"](http://c.learncodethehardway.org/book/).

For his awesome debug macros (The `dbg.h` file).

## Contributing

1. Fork it
2. Create a feature branch with a meaningful name (`git checkout -b my-new-feature`)
3. Add yourself to the CONTRIBUTORS file
4. Commit your changes (`git commit -am 'Add some feature'`)
5. Push to your branch (`git push origin my-new-feature`)
6. Create a new pull request

## Useful Stuff

I wrote one function in this library which could be more useful for many people than the library: the **char \*getFilename(const char \*input)** function, which returns the filename of a path to a file.

E.g.

```
char *path = "/var/www/html/index.html";
printf("%s\n", getFilename(path)); // Will print 'index.html'
```
