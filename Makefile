CFLAGS=-g -Wall -DNDEBUG

all: rsl-download-test rsl-upload-test

rsl-download-test: rangitaki-sync.o libssh.so

rsl-upload-test: rangitaki-sync.o libssh.so

clean:
	rm -r rangitaki-sync.o  rsl-download-test rsl-upload-test
